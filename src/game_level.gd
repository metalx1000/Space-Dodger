extends Node2D

var objects = [
	"enemy",
	"enemy_2"
]

var rng = RandomNumberGenerator.new()
var width

func _ready():
	width = get_viewport_rect().size.x
	
func _process(delta):
	hud_update()
	
func hud_update():
	$HUD.text = "LEVEL: " + str(Global.level / 10)
	$HUD.text += "\nDISTANCE: " + str(Global.distance)
	$HUD.text += "\nSCORE: " + str(Global.score)
	
func new_enemy():
	var obj = objects[randi() % objects.size()]
	var resource = "res://assets/objs/" + obj + ".tscn"
	resource = load(resource)
	var object = resource.instance()
	
	rng.randomize()
	var x = rng.randf_range(64, width-64)
	add_child(object)
	object.position = Vector2(x,-100)


func _on_new_item_timeout():
	if rng.randf_range(0, 100) < Global.level:
		new_enemy()
