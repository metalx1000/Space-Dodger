extends Node


var time_start = 0
var time_now = 0
var level = 0
var score = 0
var points = 0
var distance = 0.0

func _ready():
	time_start = OS.get_unix_time()
	pass # Replace with function body.


func _process(delta):
	time_now = OS.get_unix_time()
	level = ((time_now - time_start)/10) + 10
	score = time_now - time_start + points
	distance += .01
