extends KinematicBody2D

var explosion = load("res://explosion.tscn")
var explode = explosion.instance()
export var speed = 200
var rng = RandomNumberGenerator.new()
var velocity = Vector2.ZERO

func _ready():
	rng.randomize()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var r = rng.randf_range(0.5, 4.0)
	velocity.y += ((speed + Global.level) * delta ) * r
	velocity = move_and_slide(velocity, Vector2.UP)

func death():
	var r = rng.randf_range(0, .2)
	yield(get_tree().create_timer(r), "timeout")
	get_tree().get_current_scene().add_child(explode)
	explode.position = position
	queue_free()


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	Global.points += 10
	queue_free()
