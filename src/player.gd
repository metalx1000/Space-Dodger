extends KinematicBody2D

var screen_size 
var stop_distance = 5
var speed = 500
var explosion = load("res://explosion.tscn")
var spawn = 0



# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	spawn -= delta
	if spawn < 0:
		$CollisionShape2D.disabled = false
		$Area2D/CollisionShape2D.disabled = false
		modulate.a = 1
		#keep player in screen
		position.x = clamp(position.x, 32, screen_size.x - 32)
		position.y = clamp(position.y, 32, screen_size.y - 32)
	else:
		$Area2D/CollisionShape2D.disabled = true
		$CollisionShape2D.disabled = true
		modulate.a = .2
		
	move_to_mouse()
	
func move_to_mouse():
	var pos = get_global_mouse_position()
	pos.y -= 64 #put player infront of pointer
		
	if position.distance_to(pos) > stop_distance:
		var move_pos = pos - position
		move_pos = move_pos.normalized()
		move_and_slide(move_pos * speed)

func death():
	spawn = 3
	var explode = explosion.instance()
	get_tree().get_current_scene().add_child(explode)
	explode.position = position
	position = Vector2(-100,-500)
	#queue_free()


func _on_Area2D_body_entered(body):
	if body.is_in_group("enemies") && spawn < 0:
		death()
		body.death()
