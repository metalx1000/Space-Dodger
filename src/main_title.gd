extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("main_title")
	$AnimationPlayer.seek(0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		print("exit")
		get_tree().quit()
	
	if Input.is_action_just_pressed("ui_accept"):	
		$AnimationPlayer.play("start")
		
func start_game():
	get_tree().change_scene("res://level.tscn")
		
